**To deploy to production:**

    1. (Deploy prisma server):
        Type: prisma login
        Type (from Prisma folder): prisma deploy -e ../config/prod.env
    2. (Fetch prisma server schema for graphql-yoga):
        Type: npm get-schema-prod
    3. (Deploy NodeJS graphql-yoga server):
        Type: heroku login
        Type (if remote source isn't already added): git remote add heroku https://git.heroku.com/fierce-taiga-77783.git
        Type: git push heroku master

**To deploy to Dev/Test (Make sure docker is installed):**

    1. (Launch docker image):
        Type (From prisma folder): docker-compose up -d
    2. (Deploy prisma to Dev or Test):
        Type (From prisma folder): prisma deploy -e ../config/dev.env
    3. (Deploy NodeJS graphql-yoga server to Dev or Test):
        Type: npm run dev
    --EXTRA--
    1. (If token is needed for prisma admin or prisma graphql playground)
        Type(From prisma folder for example): prisma token -e ../config/dev.env
        With that token, put it in { "Authorization" : "Bearer token_here" } and place in http headers of playground or place token in admin settings.
    2. (If you want to access the local database from pgAdmin)
        host: localhost
        port: 5432
        maintenance db: postgres
        username: prisma
        password: prisma
        -- click save credentials --
        Database will be under prisma, schemas, gardiloo$dev
        
gardiloo api endpoint: https://fierce-taiga-77783.herokuapp.com/

prisma server endpoint (not accessible without PRISMA_SECRET): https://gardiloo-app-server-63072ab51f.herokuapp.com/gardiloo-graphql-api/prod