import '@babel/polyfill/noConflict'
import server from './server'

server.start(
    {
        port: process.env.PORT || 4000,
        cors: {
            credentials: true,
            origin: process.env.FRONTEND_URI
        },
        playground: process.env.NODE_ENV === "production" ? false : "/"
    }, 
    () => {
        console.log('The server is up!')
    }
);
