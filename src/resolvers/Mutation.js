import bcrypt from 'bcryptjs'
import getUserId from '../utils/getUserId'
import generateToken from '../utils/generateToken'
import hashPassword from '../utils/hashPassword'

const Mutation = {
    async createUser(parent, args, { prisma }, info) {
        const exists = await prisma.exists.User({ email: args.data.email });

        if (exists) {
            throw new Error('This email is already taken')
        }

        const password = await hashPassword(args.data.password)
        const user = await prisma.mutation.createUser({
            data: {
                ...args.data,
                password
            }
        })

        return {
            user,
            token: generateToken(user.id)
        }
    },
    async login(parent, args, { prisma }, info) {
        const user = await prisma.query.user({
            where: {
                email: args.data.email
            }
        })

        if (!user) {
            throw new Error('Unable to login')
        }

        const isMatch = await bcrypt.compare(args.data.password, user.password)

        if (!isMatch) {
            throw new Error('Unable to login')
        }

        return {
            user,
            token: generateToken(user.id)
        }
    },
    async deleteUser(parent, args, { prisma, request }, info) {
        const userId = getUserId(request)

        return prisma.mutation.deleteUser({
            where: {
                id: userId
            }
        }, info)
    },
    async updateUser(parent, args, { prisma, request }, info) {
        const userId = getUserId(request)

        if (typeof args.data.password === 'string') {
            args.data.password = await hashPassword(args.data.password)
        }

        return prisma.mutation.updateUser({
            where: {
                id: userId
            },
            data: args.data
        }, info)
    },
    async createDraft(parent, args, { prisma, request }, info) {
        const userId = getUserId(request)

        return prisma.mutation.createPost({
            data: {
                isbn: args.data.isbn,
                content: args.data.content,
                image: args.data.image,
                price: args.data.price,
                title: args.data.title,
                author: {
                    connect: {
                        id: userId
                    }
                }
            }
        }, info);
    },
    async deletePost(parent, args, { prisma, request }, info) {
        const userId = getUserId(request)

        return prisma.mutation.deletePost({
            where: {
                id: args.data.id
            }
        }, info);
    },
    async publish(parent, args, { prisma, request }, info) {
        const userId = getUserId(request);

        return prisma.mutation.updatePost({
            where: {
                id: args.data.id
            },
            data: {
                price: args.data.price
            }
        }, info);
    }
}

export { Mutation as default }