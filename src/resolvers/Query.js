import getUserId from '../utils/getUserId'

const Query = {
    // users(parent, args, { prisma }, info) {
    //     const opArgs = {
    //         first: args.first,
    //         skip: args.skip,
    //         after: args.after,
    //         orderBy: args.orderBy
    //     }
    //
    //     if (args.query) {
    //         opArgs.where = {
    //             OR: [{
    //                 name_contains: args.query
    //             }]
    //         }
    //     }
    //
    //     return prisma.query.users(opArgs, info)
    // },
    me(parent, args, { prisma, request }, info) {
        const userId = getUserId(request)
        
        return prisma.query.user({
            where: {
                id: userId 
            }
        })
    },
    feed(parent, args, { prisma }, info) {        
        return prisma.query.posts({
            orderBy: "createdAt_DESC"
        }, info);
    },
    filterPosts(parent, args, { prisma }, info) {
        return prisma.query.posts({
            where: {
                OR: [
                    {
                        title_contains: args.searchString,
                    },
                    {
                        isbn_contains: args.searchString,
                    }
                ]
            },
            orderBy: "createdAt_DESC"
        }, info);
    },
    post(parent, args, { prisma }, info) {
        return prisma.query.post({
            where: {
                isbn: args.isbn
            }
        }, info);
    },
    userPosts(parent, args, { prisma, request }, info) {
        const userId = getUserId(request);
        
        return prisma.query.posts({
            where: {
                author: {
                    id: userId
                }
            }
        });
    }
}

export { Query as default }
